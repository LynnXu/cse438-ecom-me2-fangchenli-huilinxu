package com.example.cse438.studio2.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cse438.studio2.R
import kotlinx.android.synthetic.main.fragment_result_list.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    private var parentContext: Context = context
    private var queryString: String = query

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayText = "Search for: $queryString"
        query_text.text = displayText

        //TODO: Finish implementing below based on HomeFragment
    }

    //TODO: Implement the custom adapter below
}